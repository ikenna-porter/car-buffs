import Reviews from './Reviews'

function MainPage() {
  return (
    <div style={{display: "flex", flexDirection: "column", width: "100vw"}}>
      {/* <div className="text-center my-1" style={{justifyContent: "center"}}>
        <h1 className="display-5 fw-bold">Car Buffs</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-1">
            The premiere solution for your automobile needs!
          </p>
        </div>
      </div> */}
      <div className="text-center" >
        <div id="carouselControls" className="carousel slide" data-ride="carousel" >
          <div className="carousel-inner" style={{width: "100vw", height: "80vh", objectFit: "contain"}}>
            <div className="carousel-item active">
              <img className="d-block w-100" src={require("./img/luxurycars3.webp")} alt="First slide" style={{maxHeight: "100%", maxWidth: "100%"}}/>
            </div>
            <div className="carousel-item">
              <img className="d-block w-100" src={require("./img/technician2.png")} alt="Second slide" style={{maxHeight: "100%", maxWidth: "100%"}}/>
            </div>
            <div className="carousel-item">
              <img className="d-block w-100" src={require("./img/saleswoman.jpeg")} alt="Third slide" style={{maxHeight: "100%", maxWidth: "100%"}}/>
            </div>
          </div>
          <a className="carousel-control-prev" href="#carouselControls" role="button" data-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="sr-only">Previous</span>
          </a>
          <a className="carousel-control-next" href="#carouselControls" role="button" data-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="sr-only">Next</span>
          </a>
        </div>
      </div>
      <Reviews />
      <footer className='bg-dark' style={{height: "7vh", maxWidth: "100%", display:"flex", justifyContent:"center", alignItems:"center", color:"white"}}>
        <i>Car Buffs - The premiere solution for your automobile needs!</i>
      </footer>
    </div>
  );
}

export default MainPage;

