
function Reviews() {
    return (
        <div className="my-3" style={{display: "flex", justifyContent: "space-evenly", padding: "20px"}}>
            <div className="card" style={{width: "18rem"}}>
                <img className="card-img-top" src={require("./img/first-car.jpeg")} alt="Card image cap" style={{height: "55%"}}/>
                <div className="card-body">
                    <p className="card-text">
                        Vilkas was a great guy and helped us step by step throughout the whole process as we purchased our son's first car! 
                        I really appreciate his honesty with us.
                    </p>
                </div>
            </div>
            <div className="card" style={{width: "18rem"}}>
                <img className="card-img-top" src={require("./img/happycustomers.jpeg")} alt="Card image cap" style={{height: "55%"}}/>
                <div className="card-body">
                    <p className="card-text">
                        The technicians are top notch. Especially Erika, who changed our AC unit's refrigerant in record time. She really saved our summer trip!
                    </p>
                </div>
            </div>
            <div className="card" style={{width: "18rem"}}> 
                <img className="card-img-top" src={require("./img/happycustomer2.webp")} alt="Card image cap" style={{height: "55%"}}/>
                <div className="card-body">
                    <p className="card-text">
                        This is a very clean car dealership and it surprisingly has a lot of cars on the lot. I will recommend this to my friends for sure!
                    </p>
                </div>
            </div>
            <div className="card" style={{width: "18rem"}}> 
                <img className="card-img-top" src={require("./img/huggingcar.jpeg")} alt="Card image cap" style={{height: "55%"}}/>
                <div className="card-body">
                    <p className="card-text">
                        I just love my new car!!! I couldn't be happier with the service that Car Buff provided!
                    </p>
                </div>
            </div>
        </div>
    );
}

export default Reviews;